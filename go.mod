module sevbit.com/golang/speedtest_server

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/gopherschool/http-rest-api v0.0.0-20190922093049-d59926c5ce3c
)
