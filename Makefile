.PHONY: build
build:
	go build -v -o speedtest_server .

.PHONY: build-bsd
build-bsd:
	GOOS=freebsd GOARCH=386 go build -v -o speedtest_server_bsd86 .

.DEFAULT_GOAL := build
