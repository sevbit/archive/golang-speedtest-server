package main

import (
	"encoding/json"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
	"time"
)

func setDefaultHeaders(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "GET, POST")

	(*w).Header().Set("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0, s-maxage=0 post-check=0, pre-check=0")
	(*w).Header().Set("Pragma", "no-cache")
}

func getIP(r *http.Request) string {
	forwarded := r.Header.Get("X-Real-IP")
	if forwarded != "" {
		return forwarded
	}
	forwarded = r.Header.Get("X-Forwarded-For")
	if forwarded != "" {
		return strings.Split(forwarded, ",")[0]
	}
	return strings.Split(r.RemoteAddr, ":")[0]
}

func getISP(ip string) string {
	url := "https://ipinfo.io/" + ip + "/json"
	client := http.Client{Timeout: time.Second * 2}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return ""
	}
	res, geterr := client.Do(req)
	if geterr != nil {
		return ""
	}
	if res.Body != nil {
		defer res.Body.Close()
	}
	body, readerr := ioutil.ReadAll(res.Body)
	if readerr != nil {
		return ""
	}
	type ISP struct {
		Org     string `json:"org"`
		Country string `json:"country"`
	}
	isp := ISP{}
	jsonerr := json.Unmarshal(body, &isp)
	if jsonerr != nil {
		return ""
	}
	return isp.Org + ", " + isp.Country
}

func isPrivateIP(ip string) bool {
	IP := net.ParseIP(ip)
	if IP == nil {
		return true
	}
	for _, cidr := range []string{
		"127.0.0.0/8",    // IPv4 loopback
		"10.0.0.0/8",     // RFC1918
		"172.16.0.0/12",  // RFC1918
		"192.168.0.0/16", // RFC1918
		"169.254.0.0/16", // RFC3927 link-local
		"::1/128",        // IPv6 loopback
		"fe80::/10",      // IPv6 link-local
		"fc00::/7",       // IPv6 unique local addr
	} {
		_, block, _ := net.ParseCIDR(cidr)
		if block.Contains(IP) {
			return true
		}
	}
	return false
}
