package main

import (
	"bufio"
	"bytes"
	"crypto/rand"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
)

var (
	bindAddr string
)

func init() {
	flag.StringVar(&bindAddr, "bind-addr", ":3000", "server listen on ip:port")
}

func info(w http.ResponseWriter, req *http.Request) {
	setDefaultHeaders(&w)
	ip := getIP(req)
	var data = map[string]string{
		"processedString": ip,
		"rawIspInfo":      "",
	}
	if !isPrivateIP(ip) {
		isp := getISP(ip)
		if isp == "" {
			isp = "Unknown ISP"
		}
		data["processedString"] += " - " + isp
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	json.NewEncoder(w).Encode(data)
}

func ping(w http.ResponseWriter, req *http.Request) {
	setDefaultHeaders(&w)
	fmt.Fprint(w, "ok")
}

func empty(w http.ResponseWriter, req *http.Request) {
	setDefaultHeaders(&w)
	w.Header().Set("Connection", "keep-alive")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Encoding, Content-Type")
	req.Body = http.MaxBytesReader(w, req.Body, 128<<20)
	defer req.Body.Close()
	scanner := bufio.NewScanner(req.Body)
	for scanner.Scan() {
		//do nothing, wait only
	}
}

func garbage(w http.ResponseWriter, req *http.Request) {
	setDefaultHeaders(&w)
	w.Header().Set("Content-Description", "File Transfer")
	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Disposition", "attachment; filename=random.dat")
	w.Header().Set("Content-Transfer-Encoding", "binary")

	data := make([]byte, 1<<20)
	rand.Read(data)

	var cksize int
	size := req.URL.Query().Get("ckSize")
	if size != "" {
		i, err := strconv.Atoi(size)
		if err == nil {
			cksize = i
		}
	}
	if cksize < 4 {
		cksize = 4
	}
	if cksize > 1024 {
		cksize = 1024
	}
	for i := 0; i < cksize; i++ {
		r := bytes.NewReader(data)
		io.Copy(w, r)
	}
}

func main() {
	flag.Parse()

	http.HandleFunc("/info", info)
	http.HandleFunc("/ping", ping)
	http.HandleFunc("/empty", empty)
	http.HandleFunc("/garbage", garbage)

	log.Println("Speedtest server start on", bindAddr)
	if err := http.ListenAndServe(bindAddr, nil); err != nil {
		log.Fatalln(err)
	}
}
